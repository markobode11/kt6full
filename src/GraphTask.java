import java.util.*;
/**
 * READ ME!
 * Henry Veetamm
 * Mallist muutmised:
 * 1) Graphi on lisatud kolm globaalset  muutujat.
 *    a) int verticesCount.
 *    b) int[][] adjMatrix
 *    c) int[][] distanceMatrix.
 * 2) Meetodi createAdjMatrix lõpus määratakse vertices ja int[][] adjMatrix.
 * 3) 9 meetodid
 *
 *Raimond Helk
 *Mallist muutmised:
 * 1) Graphi on lisatud üks globaalne muutuja.
 *    a)List<Vertex> vertices.
 * 2) Meetodi createVertex lõpus listakse loodud vertex vertices listi.
 * 3) Üks meetod getVertexEccentricity(Vertex s).
 *
 * Marko Bode
 *Mallist muutmised:
 * 1) Vertexi klassi on lisatud globaalne muutuja eccentricity.
 * 2) createRandomSimpleGraph'is lisab loodud vertex'id vertices listi.
 * 3) Algse 5 meetodi asemel on lisatud 2.
 */

/** Container class to different classes, that makes the whole
 * set of classes one class formally.
 */
public class GraphTask {

   /** Main method. */
   public static void main (String[] args) {
      GraphTask a = new GraphTask();
      System.out.println("\n".repeat(2) + "-".repeat(25) + "Example 1" + "-".repeat(25) + "\n");
      a.run1();
      System.out.println("\n".repeat(2) + "-".repeat(25) + "Example 2" + "-".repeat(25) + "\n");
      a.run2();
      System.out.println("\n".repeat(2) +"-".repeat(25) + "Example 3" + "-".repeat(25) + "\n");
      a.run3();


   }

   /** Actual main method to run examples and everything. */
   public void run1() {

      Graph g = new Graph("G");
      Vertex v1 = g.createVertex("1"); Vertex v2 = g.createVertex("2");
      Vertex v3 = g.createVertex("3"); Vertex v4 = g.createVertex("4");
      Vertex v5 = g.createVertex("5"); Vertex v6 = g.createVertex("6");
      g.createArc("1-2", v1, v2); g.createArc("2-1", v2, v1); g.createArc("2-3", v2, v3);
      g.createArc("3-2", v3, v2); g.createArc("2-4", v2, v4); g.createArc("4-2", v4, v2);
      g.createArc("3-5", v3, v5); g.createArc("5-3", v5, v3); g.createArc("4-6", v4, v6);
      g.createArc("6-4", v6, v4);

      System.out.println("Marko, Center:" + g.getCenterAsString());
      System.out.println("Henry, Diameter: " + g.graphDiameter());// Leiab koheselt graafi diameetri

      /*//Manually
      g.createAdjMatrix(); // Peab külgnevusmaatriksi tegema.
      g.makeDistanceMatrix(); // Kauguste maatriksi tegemine.
      g.shortestPaths(); // Lühimate teede maatriks, kirjutab DistanceMatrixi.
      g.findGraphDiameter*/

      System.out.println("Raimond, eccentricity:");
      for (Vertex v: g.vertices) {
         System.out.println("Vertex " + v.toString() + " eccentricity: " + g.getVertexEccentricity(v));
      }

   }
   public void run2() {

      Graph g = new Graph("G");
      Vertex v1 = g.createVertex("1"); Vertex v2 = g.createVertex("2");
      Vertex v3 = g.createVertex("3"); Vertex v4 = g.createVertex("4");
      Vertex v5 = g.createVertex("5");
      g.createArc("1-5", v1, v5); g.createArc("5-1", v5, v1); g.createArc("2-3", v2, v3);
      g.createArc("3-2", v3, v2); g.createArc("2-5", v2, v5); g.createArc("5-2", v5, v2);
      g.createArc("4-1", v4, v1); g.createArc("1-4", v1, v4);
      g.createArc("4-3", v4, v3); g.createArc("2-4", v3, v4);


      System.out.println("Marko, Center:" + g.getCenterAsString());
      System.out.println("Henry, Diameter: " + g.graphDiameter());// Leiab koheselt graafi diameetri

      /*//Manually
      g.createAdjMatrix(); // Peab külgnevusmaatriksi tegema.
      g.makeDistanceMatrix(); // Kauguste maatriksi tegemine.
      g.shortestPaths(); // Lühimate teede maatriks, kirjutab DistanceMatrixi.
      g.findGraphDiameter*/
      System.out.println("Raimond, eccentricity:");
      for (Vertex v: g.vertices) {
         System.out.println("Vertex " + v.toString() + " eccentricity: " + g.getVertexEccentricity(v));
      }
   }

   public void run3() {

      Graph g = new Graph("G");
      g.createRandomSimpleGraph(6, 7);
      System.out.println("Marko, Center:" + g.getCenterAsString());
      System.out.println("Henry, Diameter: " + g.graphDiameter());// Leiab koheselt graafi diameetri
      System.out.println("Raimond, eccentricity:");
      for (Vertex v: g.vertices) {
         System.out.println("Vertex " + v.toString() + " eccentricity: " + g.getVertexEccentricity(v));
      }
   }

   /**
    * Vertex represents a fundamental unit of which graphs are made.
    */
   class Vertex {

      /**
       * Vertex id is vertex's name.
       */
      private String id;
      /**
       * Next is next vertex from another vertex.
       */
      private Vertex next;
      /**
       * First shows the first arc for this vertex.
       */
      private Arc first;
      /**
       * Info field is for other information.
       */
      private int info = 0;
      /**
       * Eccentricity of the vertex.
       * If 0 then no eccentricity is defined yet.
       */
      private int eccentricity;



      /**
       * Constructor for creating vertex.
       * @param s Vertex's id in String.
       * @param v Vertex's next vertex in graph.
       * @param e Vertex's first arc in graph.
       */
      Vertex (String s, Vertex v, Arc e) {
         id = s;
         next = v;
         first = e;
      }

      /**
       * Constructor for creating vertex.
       * @param s Vertex's id in string.
       */
      Vertex (String s) {
         this (s, null, null);
      }

      /**
       * Creates String from Vertex.
       * @return Vertex id.
       */
      @Override
      public String toString() {
         return id;
      }


   }


   /** Arc represents one arrow in the graph. Two-directional edges are
    * represented by two Arc objects (for both directions).
    */
   class Arc {

      /**
       * Id is the name of the Arc.
       */
      private String id;
      /**
       * Target shows where the Arc is heading.
       */
      private Vertex target;
      /**
       * Next shows next Arc from this Arc.
       */
      private Arc next;


      /**
       * Constructor for creating Arc.
       * @param s Arc's id is the name for the Arc.
       * @param v Target where the Arc is heading.
       * @param a Next is the next Arc from this Arc.
       */
      Arc (String s, Vertex v, Arc a) {
         id = s;
         target = v;
         next = a;
      }

      /**
       * Constructor for creating Arc.
       * @param s Arc's id is the name for the Arc.
       */
      Arc (String s) {
         this (s, null, null);
      }

      /**
       * Creates a String form Arc.
       * @return String presentation of Arc.
       */
      @Override
      public String toString() {
         return id;
      }

   }

   /**
    * Graph is the structure for modelling arcs and vertices.
    */
   class Graph {

      /**
       * Graph id is the name for the Graph.
       */
      private String id;
      /**
       * First is the first Vertex for the Graph.
       */
      private Vertex first;
      /**
       * Vertices show how many vertices does this graph have.
       */
      private int verticesCount;
      /**
       *Adjacency matrix for this Graph, which is initially unset.
       * Which shows does some vertex u have connection with any vertex v in this Graph.
       */
      private int[][]adjMatrix;
      /**
       * Distance matrix for this Graph, which is initially unset.
       * Shows the shortest paths from vertex u to any vertex v in this Graph.
       */
      private int[][]distanceMatrix;
      /**
       * List of graphs vertices.
       */
      private List<Vertex> vertices = new ArrayList<Vertex>();

      /**
       * Construcotr for creating the graph.
       * @param s Graphs id is the name for the Graph.
       * @param v V is tehe first vertex in this Graph.
       */
      Graph (String s, Vertex v) {
         id = s;
         first = v;


      }

      /**
       * Constructor for creating the Graph.
       * @param s Graphs id is the name for the Graph.
       */
      Graph (String s) {
         this (s, null);
      }

      /**
       * Creates a string from Graph.
       * @return String representation of the Graph.
       */
      @Override
      public String toString() {
         String nl = System.getProperty ("line.separator");
         StringBuilder sb = new StringBuilder(nl);
         sb.append (id);
         sb.append (nl);
         Vertex v = first;
         while (v != null) {
            sb.append (v.toString());
            sb.append (" -->");
            Arc a = v.first;
            while (a != null) {
               sb.append (" ");
               sb.append (a.toString());
               sb.append (" (");
               sb.append (v.toString());
               sb.append ("->");
               sb.append (a.target.toString());
               sb.append (")");
               a = a.next;
            }
            sb.append (nl);
            v = v.next;
         }
         return sb.toString();
      }

      /**
       * Creates a vertex for the Graph.
       * @param vid Vid is the id for the vertex.
       * @return Returns the created Vertex.
       */
      public Vertex createVertex (String vid) {
         Vertex res = new Vertex (vid);
         res.next = first;
         first = res;
         vertices.add(res);
         return res;
      }

      /**
       * Creates a arc for the Graph.
       * @param aid aid is the name for the Arc.
       * @param from From which vertex the Arc is created for.
       * @param to To which vertex the Arc is created for.
       * @return returns the created Arc.
       */
      public Arc createArc (String aid, Vertex from, Vertex to) {
         Arc res = new Arc (aid);
         res.next = from.first;
         from.first = res;
         res.target = to;
         return res;
      }

      /**
       * Create a connected undirected random tree with n vertices.
       * Each new vertex is connected to some random existing vertex.
       * @param n number of vertices added to this graph
       */
      public void createRandomTree (int n) {
         if (n <= 0)
            return;
         Vertex[] varray = new Vertex [n];
         for (int i = 0; i < n; i++) {
            varray [i] = createVertex ("v" + String.valueOf(n-i));
            if (i > 0) {
               int vnr = (int)(Math.random()*i);
               createArc ("a" + varray [vnr].toString() + "_"
                  + varray [i].toString(), varray [vnr], varray [i]);
               createArc ("a" + varray [i].toString() + "_"
                  + varray [vnr].toString(), varray [i], varray [vnr]);
            }
         }
      }

      /**
       * Create an adjacency matrix of this graph.
       * Side effect: corrupts info fields in the graph
       * @return adjacency matrix
       */
      public int[][] createAdjMatrix() {
         int info = 0;
         Vertex v = first;
         while (v != null) {
            v.info = info++;
            v = v.next;
         }
         int[][] res = new int [info][info];
         v = first;
         while (v != null) {
            int i = v.info;
            Arc a = v.first;
            while (a != null) {
               int j = a.target.info;
               res [i][j]++;
               a = a.next;
            }
            v = v.next;
         }
         this.adjMatrix = res;
         verticesCount = adjMatrix.length;
         return res;
      }

      /**
       * Create a connected simple (undirected, no loops, no multiple
       * arcs) random graph with n vertices and m edges.
       * @param n number of vertices
       * @param m number of edges
       */
      public void createRandomSimpleGraph (int n, int m) {
         if (n <= 0)
            return;
         if (n > 2500)
            throw new IllegalArgumentException ("Too many vertices: " + n);
         if (m < n-1 || m > n*(n-1)/2)
            throw new IllegalArgumentException 
               ("Impossible number of edges: " + m);
         first = null;
         createRandomTree (n);       // n-1 edges created here
         Vertex[] vert = new Vertex [n];
         Vertex v = first;
         int c = 0;
         while (v != null) {
            vert[c++] = v;
            v = v.next;
         }
         int[][] connected = createAdjMatrix();
         int edgeCount = m - n + 1;  // remaining edges
         while (edgeCount > 0) {
            int i = (int)(Math.random()*n);  // random source
            int j = (int)(Math.random()*n);  // random target
            if (i==j) 
               continue;  // no loops
            if (connected [i][j] != 0 || connected [j][i] != 0) 
               continue;  // no multiple edges
            Vertex vi = vert [i];
            Vertex vj = vert [j];
            createArc ("a" + vi.toString() + "_" + vj.toString(), vi, vj);
            connected [i][j] = 1;
            createArc ("a" + vj.toString() + "_" + vi.toString(), vj, vi);
            connected [j][i] = 1;
            edgeCount--;  // a new edge happily created
         }
      }


      /**
       * LISATUD: HENRY VEETAMM.
       */

      /**
       * Finds the maximum value from distance matrix.
       * In other words finds the greatest eccentricity for the graph.
       * @return Returns the graph diameter.
       */
      public int findGraphDiameter(){
         int maxValue = 0;
         for(int i = 0; i < verticesCount; i++){
            for(int j = 0; j < verticesCount; j++){
               if (distanceMatrix[i][j] > maxValue){
                  maxValue = distanceMatrix[i][j];
               }
            }
         }
         return maxValue;
      }

      /**
       * This method returns diameter in string
       * @return String presentation of Graphs's diameter
       */
      public String graphDiameter(){
         createAdjMatrix();
         makeDistanceMatrix();
         shortestPaths();
         int diameter = findGraphDiameter();
         return "Graph " + id + " diameter:" + diameter;
      }

      /**
       * Finds the shortest paths from vertex u to every vertex v.
       * Shortest paths are calculated using Floyd-Warshall algorithm.
       * If it founds a shorter way from vertex u to vertex v, then it sets a new value for it.
       */
      public void shortestPaths() {
         makeDistanceMatrix();
         int n = verticesCount;
         if (n < 1) return;
         for (int k=0; k<n; k++) {
            for (int i=0; i<n; i++) {
               for (int j=0; j<n; j++) {
                  int newlength = getFromMatrix (i, k) + getFromMatrix (k, j);
                  if (getFromMatrix (i, j) > newlength) {
                     setIntoMatrix (i, j, newlength);
                  }
               }
            }
         }
      }

      /**
       * Creates a initial distance matrix for every vertex in adjacency matrix.
       * If value in adjacency matrix is zero, the distance is infinity.
       * Finally sets Graph's global variable distanceMatrix because initially
       * distanceMatrix is undefined.
       */
      public void makeDistanceMatrix(){
         int INFINITY = Integer.MAX_VALUE / 2;
         int[][] disMatrix = copyMatrix();
         for (int row = 0; row < verticesCount; row ++){
            for (int column = 0; column < verticesCount; column ++){
               if( row != column && disMatrix[row][column] != 1){
                  disMatrix[row][column] = INFINITY;
               }
            }
         }
         this.distanceMatrix = disMatrix;
      }

      /**
       * Creates a copy of the adjacency matrix.
       * @return copy of the adjacency matrix.
       */
      public int[][] copyMatrix(){
         int[][] copied = new int[verticesCount][verticesCount];
         for (int row = 0; row < verticesCount; row ++){
            System.arraycopy(adjMatrix[row], 0, copied[row], 0, verticesCount);
         }
         return copied;
      }

      /**
       * This method works only for distance matrix.
       * returns value from distance matrix.
       * @param row row number.
       * @param column column number.
       * @return Value from distance matrix.
       */
      public int getFromMatrix(int row, int column){
         return distanceMatrix[row][column];
      }

      /**
       * This method works only for distance matrix.
       * Sets a new value into the distance matrix.
       * @param row row number.
       * @param column column number.
       * @param newValue matrix's new value what is set.
       */
      public void setIntoMatrix(int row, int column, int newValue){
         distanceMatrix[row][column] = newValue;
      }

      /**
       * Creates string from adjacency matrix.
       * @return String representation of adjacecny matrix.
       */
      public String getAdjMatrix(){
         StringBuilder finalString = new StringBuilder();
         for (int i = 0; i < verticesCount; i++) {
            finalString.append(Arrays.toString(adjMatrix[i])).append("\n");
         }
         return finalString.toString();
      }

      /**
       * Creates a string from distance matrix.
       * @return String representation of distance matrix.
       */
      public String getDistanceMatrix(){
         StringBuilder finalString = new StringBuilder();
         for (int i = 0; i < verticesCount; i++) {
            finalString.append(Arrays.toString(distanceMatrix[i])).append("\n");
         }
         return finalString.toString();
      }


      /**
       * LISATUD: RAIMOND HELK.
       */

      /**
       * Get the given vertex's eccentricity in this graph
       * using the Breadth-First search algorithm,
       * assuming its a connected simple graph.
       * Sources: https://en.wikipedia.org/wiki/Breadth-first_search
       * @param s source vertex
       * @return eccentricity of the source vertex
       */
      public int getVertexEccentricity(Vertex s) {
         if (!vertices.contains(s))
            throw new RuntimeException("Given vertex is not in this graph!");

         createAdjMatrix();
         int sourceIndex = s.info;
         boolean[] isVisited = new boolean[adjMatrix.length];
         int[] distances = new int[adjMatrix.length];
         int eccentricity = 0;

         LinkedList<Integer> queue = new LinkedList<>();
         isVisited[sourceIndex] = true;
         queue.add(sourceIndex);

         while (!queue.isEmpty()) {
            int v = queue.remove();
            for (int i = 0; i < adjMatrix.length; i++) {
               if ((adjMatrix[v][i] == 1) && (!isVisited[i])) {
                  isVisited[i] = true;
                  queue.add(i);
                  distances[i] = distances[v] + 1;
               }
            }
         }

         for (int i = 0; i < adjMatrix.length; i++) {
            if (distances[i] == 0 && i != sourceIndex)
               throw new RuntimeException("Cannot find path to every vertex! Connected simple graph required!");
            if (distances[i] > eccentricity)
               eccentricity = distances[i];
         }
         return eccentricity;
      }


      /**
       * LISATUD: MARKO BODE.
       */

      /**
       * Find the center of this graph. Center is the vertex which eccentricity is the smallest.
       * If several vertices share the smallest eccentricity then all these vertices are a part of the center.
       *
       * @return center of the graph as a list of vertices
       */
      public List<Vertex> getCenter() {
         List<Vertex> center = new ArrayList<>();

         if (vertices.size() < 1) {
            return center;
         } else if (vertices.size() == 1) {
            center.add(vertices.get(0));
            return center;
         }

         shortestPaths();

         for (Vertex vertex : vertices) {
            vertex.eccentricity = getVertexEccentricity(vertex);
            if (center.size() == 0) {
               center.add(vertex);
            } else if (vertex.eccentricity < center.get(0).eccentricity) {
               center = new ArrayList<>();
               center.add(vertex);
            } else if (vertex.eccentricity == center.get(0).eccentricity) {
               center.add(vertex);
            }
         }
         return center;
      }

      /**
       * Get the center of this Graph as a string.
       *
       * @return sentence to describe the center of this Graph
       */
      public String getCenterAsString() {
         List<Vertex> center = getCenter();
         String result = this.toString() + "\n";
         if (center.size() == 0) {
            result += "Graph with no vertices does not have a center!";
         } else if (center.size() == 1) {
            result += "The center of Graph " + id + "(shown above) is " + center.get(0);
         } else {
            StringBuilder vertices = new StringBuilder();
            for (Vertex vertex : center) {
               if (center.indexOf(vertex) != center.size() - 1) {
                  vertices.append(vertex);
                  vertices.append(", ");

               } else {
                  vertices.append(vertex);
               }

            }
            result += "The center of Graph " + id + " (shown above) are vertices " + vertices.toString();
         }
         return result;
      }
   }

} 
